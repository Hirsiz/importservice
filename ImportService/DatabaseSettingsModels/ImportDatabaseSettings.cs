public class ImportDatabaseSettings : IImportDatabaseSettings
{
    public string ImportCollectionName { get; set; }
    public string FilesCollectionName { get; set; }
    public string ConnectionString { get; set; }
    public string DatabaseName { get; set; }
}