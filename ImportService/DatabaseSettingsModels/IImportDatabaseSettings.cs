public interface IImportDatabaseSettings
{
    string ImportCollectionName { get; set; }
    string FilesCollectionName { get; set; }
    string ConnectionString { get; set; }
    string DatabaseName { get; set; }
}