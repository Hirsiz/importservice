using System;

namespace ImportService.Models.Record.Family
{
    public class Marriage
    {
        public string Place { get; set; } = Constants.DEFAULT_VALUE;
        public DateTime Date { get; set; }

    }
}