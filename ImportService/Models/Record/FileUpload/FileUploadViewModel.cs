using Microsoft.AspNetCore.Http;

namespace ImportService.Models.Record.FileUpload
{
    public class FileUploadViewModel
    {
        public string Extension { get; set; }
        public IFormFile File { get; set; }
        public long Size { get; set; }
        public string Source { get; set; }
    }
}