using ImportService.Models.Record.Individual;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ImportService.Models.Record.Family;
using System.Collections.Generic;
using System;

public class Record
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string ID { get; set; }
    public List<FamilyModel> Family { get; set; }
    public IndividualModel Individual { get; set; }
    public string FileID { get; set; }
    public InformationModel Information { get; set; }
    public Guid AccessToken { get; set; }
    public string MaintainerGroupID { get; set; }
    public string VisibilityGroupID { get; set; }
    
}