using System.Collections.Generic;
using ImportService.Models.Record.Family;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ImportService.Models.Record.Individual
{
    public class IndividualModel
    {
        public IndividualModel()
        {
            ID = ObjectId.GenerateNewId();
        }

        [BsonId]
        [BsonElement("ID")]
        public ObjectId ID { get; set; }
        public string Name { get; set; } = Constants.DEFAULT_VALUE;
        public string Gender { get; set; } = Constants.DEFAULT_VALUE;
        public AddressModel Address { get; set; } = new AddressModel();
        public BirthModel Birth { get; set; } = new BirthModel();
        public DeathModel Death { get; set; } = new DeathModel();
        public bool IsAncestor { get; set; }
        //add fact object
    }
}