using System;

namespace ImportService.Models.Record.Individual
{
    public class DeathModel
    {
        public string Place { get; set; } = Constants.DEFAULT_VALUE;
        public string Date { get; set; } = Constants.DEFAULT_VALUE;
    }
}