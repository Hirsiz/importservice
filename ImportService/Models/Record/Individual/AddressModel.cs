namespace ImportService.Models.Record.Individual
{
    public class AddressModel
    {
        public string AddressLine { get; set; } = Constants.DEFAULT_VALUE;
        public string ContiuedAddress { get; set; } = Constants.DEFAULT_VALUE;
    }
}