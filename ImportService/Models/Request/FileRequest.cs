using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

public class FileRequest
{
    public IFormFile Body { get; set; }
    public Guid AccessToken { get; set; }
    public string Name { get; set; }
    public List<int> PermissionIDs { get; set; }
    public List<int> VisibilityIDs { get; set; }
}