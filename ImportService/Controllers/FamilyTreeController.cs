using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
public class FamilyTreeController : ControllerBase
{
    public readonly IFamilyTreeManager __FamilyTreeManager;

    public FamilyTreeController(IFamilyTreeManager familyTreeManager)
    {
        __FamilyTreeManager = familyTreeManager;
    }

    // GET api/values
    [HttpGet]
    public ActionResult<IEnumerable<Record>> Get()
    {
       return __FamilyTreeManager.GetRecords();
    }
}