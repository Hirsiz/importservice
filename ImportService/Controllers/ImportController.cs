﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportService.Managers.ImportManager;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ImportService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportController : ControllerBase
    {
        public readonly IImportManager __ImportManager;
        public readonly IFileManager __FileManager;

        public ImportController(IImportManager importManager,
        IFileManager fileManager)
        {
            __ImportManager = importManager;
            __FileManager = fileManager;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FileEntity>> Get(Guid accessToken)
        {
            return __FileManager.GetFiles(accessToken);
        }

        [HttpPost]
        public ActionResult<IEnumerable<FileEntity>> Post(
            [FromForm(Name = "body")]IFormFile body,
            [FromForm(Name = "accessToken")]Guid accessToken,
            [FromForm(Name = "treeName")] string name,
            [FromForm(Name = "maintainerID")] string maintainerID,
            [FromForm(Name = "visibilityID")] string visibilityID
            )
        {
            if (body.Length > 0)
            {
                var result = new StringBuilder();
                using (var reader = new StreamReader(body.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                    {
                        result.AppendLine(reader.ReadLine());
                    }
                }
                FileEntity _File = __FileManager.StoreFile(body, accessToken, name, maintainerID, visibilityID);
                __ImportManager.Import(result, _File, accessToken, maintainerID, visibilityID);
            }
            return __FileManager.GetFiles(accessToken);
        }

        [HttpDelete]
        public ActionResult<List<FileEntity>> Delete([FromBody]string id)
        {
            return __FileManager.DeleteFile(id);
        }
    }
}
