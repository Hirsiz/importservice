using System.Collections.Generic;
using MongoDB.Driver;

public class FileRepository : IFileRepository
{
    private readonly IMongoCollection<FileEntity> __Files;

    public FileRepository(IImportDatabaseSettings settings)
    {
        MongoClient _Client = new MongoClient(settings.ConnectionString);
        IMongoDatabase _Database = _Client.GetDatabase(settings.DatabaseName);

        __Files = _Database.GetCollection<FileEntity>(settings.FilesCollectionName);
    }


    public List<FileEntity> Get()
    {
        return __Files.Find(file => true).ToList();
    }

    public FileEntity Create(FileEntity file)
    {
        __Files.InsertOne(file);
        return file;
    }

    public List<FileEntity> Remove(string id)
    {
         __Files.DeleteOne(file => file.ID == id);
         return Get();
    }
}