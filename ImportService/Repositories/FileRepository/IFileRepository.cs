using System.Collections.Generic;

public interface IFileRepository
{
    List<FileEntity> Get();
    FileEntity Create(FileEntity file);
     List<FileEntity> Remove(string id);
}
