using System;
using System.Text;

namespace ImportService.Managers.ImportManager
{
    public interface IImportManager
    {
        bool Import(StringBuilder fileLines, FileEntity file, Guid accessToken, string maintainerGroupID, string visibiltyGroupID);
    }
}