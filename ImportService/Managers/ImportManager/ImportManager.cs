using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ImportService.Models.Record.Family;
using ImportService.Models.Record.Individual;

namespace ImportService.Managers.ImportManager
{
    public class ImportManager : IImportManager
    {
        private const int NAME_INDEX = 7;
        private const int GENDER_INDEX = 6;
        private const int BIRTH_PLACE_INDEX = 7;
        private const int BIRTH_DATE_INDEX = 7;
        private const int FAMILY_INDEX = 7;

        private readonly IRecordRepository __RecordRepository;
        private List<FamilyModel> __Families;
        private List<FamilyModel> __RunningList;

        private int __MaleCount = 0;
        private int __FemaleCount = 0;
        private int __UnknownCount = 0;


        public ImportManager(IRecordRepository recordRepository)
        {
            __RecordRepository = recordRepository;
            __Families = new List<FamilyModel>();
            __RunningList = new List<FamilyModel>();
        }

        public bool Import(StringBuilder fileLines, FileEntity file, Guid accessToken, string maintainerGroupID, string visibiltyGroupID)
        {
            string[] _FileLines = fileLines.ToString().Split("\n");
            Record _Record = new Record();
            for (int i = 0; i < _FileLines.Length; i++)
            {
                if (_FileLines[i] != string.Empty && _FileLines[i][0].ToString() == "0")
                {
                    if (_FileLines[i].Contains("INDI"))
                    {
                        ProcessFamilies(i, _FileLines);
                    }
                }
            }
            _Record.FileID = file.ID;
            _Record.Family = __Families;
            __Families[0].Father.IsAncestor = true;
            _Record.Information = new InformationModel()
            {
                MaleCount = __MaleCount,
                FemaleCount = __FemaleCount,
                UnknownCount = __UnknownCount
            };
            _Record.AccessToken = accessToken;
            _Record.MaintainerGroupID = maintainerGroupID;
            _Record.VisibilityGroupID = visibiltyGroupID;

            __RecordRepository.Create(_Record);
            return true;
        }

        // private FamilyModel CheckFamilies(List<FamilyModel> families, IndividualModel individual)
        // {
        //     FamilyModel _Test = families.Where(family => family.Father.ID == individual.ID || family.Mother.ID == individual.ID).SingleOrDefault();
        //     if (_Test == null)
        //     {
        //         List<FamilyModel> _Children = families.Select(family => family.ChildrenFamily).ToList();
        //         _Test = CheckFamily(families, individual);
        //     }
        //     return _Test;
        // }

        private FamilyModel RetrieveSpouseFamilyByIndividualID(FamilyModel family, IndividualModel individual)
        {
            if (family.Father?.ID == individual.ID || family.Mother?.ID == individual.ID)
            {
                return family;
            }
            else if (family.ChildrenFamily.Count != 0)
            {
                FamilyModel _ChildrenFamily = new FamilyModel();
                foreach (FamilyModel childFamily in family.ChildrenFamily)
                {
                    _ChildrenFamily = RetrieveSpouseFamilyByIndividualID(childFamily, individual);
                    if (_ChildrenFamily != null)
                    {
                        return _ChildrenFamily;
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }

        private FamilyModel RetreiveSpouseFamilyByFamilyName(FamilyModel family, string familyName)
        {
            if (family.FamilyName == familyName)
            {
                return family;
            }
            else if (family.ChildrenFamily.Count != 0)
            {
                FamilyModel _Test = new FamilyModel();
                foreach (FamilyModel childFamily in family.ChildrenFamily)
                {
                    _Test = RetreiveSpouseFamilyByFamilyName(childFamily, familyName);
                    if (_Test != null)
                    {
                        return _Test;
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }

        private void ProcessFamilies(int index, string[] fileLines)
        {
            IndividualModel _Individual = new IndividualModel();
            index++;
            while (fileLines[index][0].ToString() != "0")
            {
                if (fileLines[index].Contains("NAME"))
                {
                    _Individual.Name = fileLines[index].Substring(NAME_INDEX).Replace("/", string.Empty);
                }
                else if (fileLines[index].Contains("SEX"))
                {
                    string _Gender = fileLines[index].Substring(GENDER_INDEX);

                    if (_Gender == "M")
                    {
                        __MaleCount++;
                    }
                    else if (_Gender == "F")
                    {
                        __FemaleCount++;
                    }
                    else
                    {
                        __UnknownCount++;
                    }
                    _Individual.Gender = _Gender;
                }

                else if (fileLines[index].Contains("BIRT"))
                {
                    index++;
                    while (fileLines[index][0].ToString() != "1")
                    {
                        if (fileLines[index].Contains("PLAC"))
                        {
                            _Individual.Birth.Place = fileLines[index].Substring(BIRTH_PLACE_INDEX);
                        }
                        if (fileLines[index].Contains("DATE"))
                        {
                            _Individual.Birth.Date = fileLines[index].Substring(BIRTH_DATE_INDEX);
                        }
                        index++;
                    }
                    index--;
                }

                else if (fileLines[index].Contains("DEAT"))
                {
                    index++;
                    while (fileLines[index][0].ToString() != "1")
                    {
                        if (fileLines[index].Contains("PLAC"))
                        {
                            _Individual.Death.Place = fileLines[index].Substring(BIRTH_PLACE_INDEX);
                        }
                        if (fileLines[index].Contains("DATE"))
                        {
                            _Individual.Death.Date = fileLines[index].Substring(BIRTH_DATE_INDEX);
                        }
                        index++;
                    }
                    index--;
                }

                else if (fileLines[index].Contains("FAMS"))
                {
                    bool _NotRootFamily = !__Families.Any(family => family.FamilyName == fileLines[index].Substring(FAMILY_INDEX));
                    if (_NotRootFamily)
                    {
                        if (__Families.Count != 0)
                        {
                            FamilyModel _SpouseFamily = RetrieveSpouseFamilyByIndividualID(__Families[0], _Individual);

                            if (_SpouseFamily == null)
                            {
                                //There could exist an individual that is not appeared in the tree yet, mainly spouses
                                _SpouseFamily = RetreiveSpouseFamilyByFamilyName(__Families[0], fileLines[index].Substring(FAMILY_INDEX));
                            }

                            if (_SpouseFamily != null)
                            {

                                _SpouseFamily.FamilyName = fileLines[index].Substring(FAMILY_INDEX);
                                if (_Individual.Gender == "M")
                                {
                                    _SpouseFamily.Father = _Individual;
                                }
                                else if (_Individual.Gender == "F")
                                {
                                    _SpouseFamily.Mother = _Individual;
                                }
                            }

                            if (_SpouseFamily == null)
                            {
                                FamilyModel _FamilyChild = new FamilyModel();
                                _FamilyChild.FamilyName = fileLines[index].Substring(FAMILY_INDEX);
                                if (_Individual.Gender == "M")
                                {
                                    _FamilyChild.Father = _Individual;
                                }
                                else if (_Individual.Gender == "F")
                                {
                                    _FamilyChild.Mother = _Individual;
                                }
                                __RunningList.Add(_FamilyChild);
                            }
                        }
                        else
                        {
                            FamilyModel _FamilySpouse = new FamilyModel();
                            _FamilySpouse.FamilyName = fileLines[index].Substring(FAMILY_INDEX);
                            if (_Individual.Gender == "M")
                            {
                                _FamilySpouse.Father = _Individual;
                            }
                            else if (_Individual.Gender == "F")
                            {
                                _FamilySpouse.Mother = _Individual;
                            }
                            __Families.Add(_FamilySpouse);
                        }
                    }
                    else
                    {
                        FamilyModel _IndividualFamily = __Families.Where(family => family.FamilyName == fileLines[index].Substring(FAMILY_INDEX)).SingleOrDefault();
                        if (_Individual.Gender == "M")
                        {
                            _IndividualFamily.Father = _Individual;
                        }
                        else if (_Individual.Gender == "F")
                        {
                            _IndividualFamily.Mother = _Individual;
                        }
                        // __RunningList.Add(_IndividualFamily);
                    }
                }

                if (fileLines[index].Contains("FAMC"))
                {
                    if (!__Families.Any(family => family.FamilyName == fileLines[index].Substring(FAMILY_INDEX)))
                    {
                        if (__Families.Count != 0)
                        {
                            FamilyModel _ChildFamily = RetreiveSpouseFamilyByFamilyName(__Families[0], fileLines[index].Substring(FAMILY_INDEX));
                            if (_ChildFamily != null)
                            {
                                if (_Individual.Gender == "M")
                                {
                                    _ChildFamily.ChildrenFamily.Add(new FamilyModel()
                                    {
                                        Father = _Individual
                                    });
                                }
                                else if (_Individual.Gender == "F")
                                {
                                    _ChildFamily.ChildrenFamily.Add(new FamilyModel()
                                    {
                                        Mother = _Individual
                                    });
                                }
                            }
                            else
                            {
                                FamilyModel _Test = null;

                                foreach (FamilyModel family in __RunningList)
                                {
                                    if (family.Father?.ID == _Individual.ID || family.Mother?.ID == _Individual.ID)
                                    {
                                        _Test = family;
                                    }
                                }

                                if (_Test != null)
                                {
                                    // _IndividualFamily.ChildrenFamily.Add(_Test);
                                }
                            }
                        }
                    }
                    else
                    {
                        FamilyModel _IndividualFamily = __Families.Where(family => family.FamilyName == fileLines[index].Substring(FAMILY_INDEX)).SingleOrDefault();
                        FamilyModel _Test = null;

                        foreach (FamilyModel family in __RunningList)
                        {
                            if (family.Father?.ID == _Individual.ID || family.Mother?.ID == _Individual.ID)
                            {
                                _Test = family;
                            }
                        }

                        if (_Test != null)
                        {
                            _IndividualFamily.ChildrenFamily.Add(_Test);
                        }

                        else
                        {
                            FamilyModel _FamilyChild = new FamilyModel();
                            _FamilyChild.FamilyName = _IndividualFamily.FamilyName;
                            if (_Individual.Gender == "M")
                            {
                                _FamilyChild.Father = _Individual;
                            }
                            else if (_Individual.Gender == "F")
                            {
                                _FamilyChild.Mother = _Individual;
                            }
                            _IndividualFamily.ChildrenFamily.Add(_FamilyChild);
                        }
                    }

                    _Individual.IsAncestor = true;
                }
                index++;
            }
            index--;
        }
    }
}