using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

public interface IFileManager
{
    FileEntity StoreFile(IFormFile file, Guid accessToken, string name, string maintainer, string visibility);
    List<FileEntity> GetFiles(Guid accessToken);
    List<FileEntity> DeleteFile(string id);
}