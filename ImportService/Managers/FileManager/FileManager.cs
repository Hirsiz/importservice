using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

public class FileManager : IFileManager
{
    private IFileRepository __FileRepository;
    private IRecordRepository __RecordRepository;

    public FileManager(IFileRepository fileRepository, IRecordRepository recordRepository)
    {
        __FileRepository = fileRepository;
        __RecordRepository = recordRepository;
    }

    public FileEntity StoreFile(IFormFile file, Guid accessToken, string name, string maintainer, string visibility)
    {
        FileEntity _File = new FileEntity()
        {
            Name = file.FileName,
            Extension = file.ContentType,
            AccessToken = accessToken,
            TreeName = name,
            MaintainerGroup = maintainer,
            VisibilityGroup = visibility
        };
        __FileRepository.Create(_File);

        return _File;
    }

    public List<FileEntity> GetFiles(Guid accessToken)
    {
        return __FileRepository.Get().Where(file => file.AccessToken == accessToken).ToList();
    }

    public List<FileEntity> DeleteFile(string id)
    {
        __RecordRepository.RemoveMany(id);
        return __FileRepository.Remove(id);
    }
}