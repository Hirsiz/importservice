using System.Collections.Generic;

public interface IFamilyTreeManager
{
    List<Record> GetRecords();
}