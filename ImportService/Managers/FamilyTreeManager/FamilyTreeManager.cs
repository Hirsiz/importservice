using System.Collections.Generic;

public class FamilyTreeManager : IFamilyTreeManager
{
    public IRecordRepository __RecordRepository;

    public  FamilyTreeManager(IRecordRepository recordRepository)
    {
        __RecordRepository = recordRepository;
    }

    public List<Record> GetRecords()
    {
        return __RecordRepository.Get();
    }
}